variable "ami_id" {
  type = string
  # default = "ami-0cdb8266fcd5d3d63" # CENTOS 8 AMI
  # default = "ami-092613d472193475e" # Golden AMI v6 Creative/Peaceful & v1.19.0
  # default = "ami-0352b4d4db9bf7bab" # Golden AMI v7 Creative/Peaceful/Offline False v1.19.0
  default = "ami-040592c74e599491b" # Golden AMI v8 Creative/Peaceful/Offline False/Nodejs v1.19.0
}

variable "instance_size" {
  type    = string
  default = "c5a.xlarge"
}

variable "mc_server_disk_size" {
  type    = number
  default = 40
}

variable "server_key" {
  type    = string
  default = "minecraft-server-key"
}

variable "remote_state_bucket" {
  type        = string
  description = "Remote State Bucket to fetch outputs from other stacks"
}

variable "server_domain_name" {
  type        = string
  description = "The main domain from which our subdomain will be created from"
}

variable "server_subdomain_name" {
  type        = string
  description = "The sub domain name the server will use to be presented in the end to the player so he can connect"
}
