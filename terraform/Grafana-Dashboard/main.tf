resource "grafana_data_source" "prometheus" {
  type = "prometheus"
  name = "TF_PROMETHEUS"
  uid  = "prometheus-tf-test-uid"
  url  = "http://localhost:9090"
}

resource "grafana_dashboard" "linux_default_dashboard" {
  config_json = file("linux-host-dashboard.json")
}

resource "grafana_dashboard" "linux_full_dashboard" {
  config_json = file("linux-host-dashboard-v2.json")
}

resource "grafana_dashboard" "linux_minecraft_dashboard" {
  config_json = file("minecraft-dashboard.json")
}